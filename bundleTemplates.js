const HandlebarsBundleBuild = require('handlebars-bundle');

const src = 'dist';
const root = 'dist';
const output = 'dist';
const watch = false;

HandlebarsBundleBuild({
  src,
  root,
  output,
  watch,
});
