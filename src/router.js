import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/views/main'
import Info from '@/views/info'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: "/",
    routes: [
        {
            path: '/',
            name: 'list',
            component: Main
        },
        {
            path: '/:id',
            name: 'info',
            component: Info
        }
    ]
})
