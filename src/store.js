import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'

const $http = axios.create({
    baseURL: 'https://swapi.co/api/'
});

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        ships: null,
        ship: null
    },
    actions: {
        async getShipList({commit}, url) {
            const {data} = await $http.get(url || '/starships');
            commit('loadShipList', data);
            return data;
        },
        async searchShip({commit}, value) {
            const {data} = await $http.get(`/starships/?search=${value}`);
            commit('loadShipList', data);
            return data;
        },
        async loadShip({commit}, id) {
            commit('loadShip', null);
            const {data} = await $http.get(`/starships/${id}`);
            commit('loadShip', data);
            return data;
        }
    },
    mutations: {
        loadShipList(state, data) {
            state.ships = data;
        },
        loadShip(state, data) {
            state.ship = data;
        }
    }
})
