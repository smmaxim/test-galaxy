const months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Авгруста', 'Сентября', 'Октября', 'Ноября', 'Декабря']
const now = new Date()

export default function (value) {
    const date = new Date(value * 1000)
    const mm = date.getMonth() // getMonth() is zero-based
    const dd = date.getDate()
    const yyyy = date.getFullYear()
    // const hours = date.getHours()
    // const mins = date.getMinutes()
    return [
        (dd > 9 ? '' : '0') + dd,
        months[mm],
        now.getFullYear() !== yyyy ? yyyy : '',
        // hours + ':' + mins
    ].join(' ');
}
