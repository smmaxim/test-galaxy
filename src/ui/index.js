import Vue from 'vue'

Vue.component('ui-btn', require('./btn').default)
Vue.component('ui-input', require('./input').default)
Vue.component('ui-spinner', require('./spinner').default)