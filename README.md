https://.stage.hzberg.com

## Project setup
```
npm install
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### DEV
docker login
sudo docker run -it -v `pwd`:/app -v ~/.ssh:/root/.ssh -p 80:8080 node:latest bash
cd app/
npm i - при необходимости
npm run serve
