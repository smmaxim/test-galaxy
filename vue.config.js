const path = require('path')

// vue.config.js
module.exports = {
    // publicPath: process.env.NODE_ENV === 'production' ? 'host/static' : './',
    publicPath: process.env.NODE_ENV === 'production' ? '/' : './',
    chainWebpack: config => {
        const types = ['vue-modules', 'vue', 'normal-modules', 'normal'];
        types.forEach(type => addStyleResource(config.module.rule('stylus').oneOf(type)));
    },
    // devServer: {
    //     proxy: {
    //         '/api': {
    //             target: 'host',
    //             secure: false,
    //             changeOrigin: true
    //         }
    //     }
    // }
}

function addStyleResource (rule) {
    rule.use('style-resource')
        .loader('style-resources-loader')
        .options({
            patterns: [
                path.resolve(__dirname, './src/assets/stylus/variables.styl'),
                path.resolve(__dirname, './src/assets/stylus/responsive.styl')
            ]
        })
}
