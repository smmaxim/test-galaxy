import Vue from 'vue'

import './src/ui/index.js'
import './src/icons/index.js'

Vue.component('ui-icons', require('./src/icons/index.vue').default)
