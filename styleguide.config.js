// Docs https://vue-styleguidist.github.io/GettingStarted.html

const path = require('path')

module.exports = {
    require: [
        path.resolve(process.cwd(), 'src/assets/stylus/main.styl'),
        path.resolve(process.cwd(), './app.styleguide.js')
    ],
    sections: [
        {
            name: 'Иконки',
            components: './src/icons/index.vue'
        },
        {
            name: 'Компоненты',
            components: './src/ui/*/*.vue'
        }
    ]
}
